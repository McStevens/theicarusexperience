Arduino code for the safety console used in the "software development project methods" course.
Property of the Icarus project group.

The libraries in the libraries folder were all written specificly for this project, 
EXCEPT for "MFRC522", wich is an open source library that can be downloaded either from the Arduino studio IDE
or from https://github.com/miguelbalboa/rfid 