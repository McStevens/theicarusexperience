/*
 * Icarus 1.0
 * This is the main file for the harware code running on the arduino board.
 * It contains the main loop calling the functions for the different functionalities of the safety console.
 * Written by Edvin Egerhag and Filip Carlsson.
 * Property of the Icarus project group.
 * Current version released October 8, 2019
*/

//Libraries used in the nuclear power plant project, consult the readme file in the libraries folder for more information
#include <config.h>         //This file contains all the defines, variables and enums used in the program.
#include <rfid.h>           //This file contains the state machine running the rfid card reader.
#include <pushButtons.h>    //This file contains the state machine for the buttons.
#include <sendRadiation.h>  //This file contains the function for sending radiation level updates.
#include <bluetooth.h>      //This file contains the statemachine that handles the bluetooth comms.



void setup()
{
  Serial.begin(SERIAL_BAUD_RATE);   // Initiate a serial communication, SERIAL_BAUD_RATE is set to 9600
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
}

//Main program loop, calls all the needed functions
void loop()
{
  Rfid(); //Checks the RFID card reader, if a new card is read, this function updates the readMsg and content variables incase a new card is detected
  
  Bt(); // This function handles the bluetooth comms, incase the content string is not empty, this function sends the readMsg and content via bluetooth. Looks for incoming messages otherwise.

  pushButtons(); //This function updates the readMsg and content string incase a button is pressed.

  sendRadiation(); //This function updates the readMsg and content string if the radiation level is adjusted.
  
}
