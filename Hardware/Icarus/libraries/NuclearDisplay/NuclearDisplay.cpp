/*
    NuclearDisplay.cpp
    Library for the LEDs on the nuclear control panel
    Created 2019-10-03 by Edvin Egerhag
    Property of the Icarus project group
*/

#include "NuclearDisplay.h"
#include "Arduino.h"

NuclearDisplay::NuclearDisplay(int g, int r, int b, int r1, int r2, int r3, int hazmatSuit)
{
    _greenLed = g;
    _redLed = r;
    _buzzer = b;
    pinMode(_greenLed, OUTPUT);
    pinMode(_redLed, OUTPUT);
    pinMode(_buzzer, OUTPUT);

    _r1 = r1;
    _r2 = r2;
    _r3 = r3;
    pinMode(_r1, OUTPUT);
    pinMode(_r2, OUTPUT);
    pinMode(_r3, OUTPUT);
    
    _hazmatSuit = hazmatSuit;
    pinMode(_hazmatSuit, OUTPUT);
    _hazmatCurrentlyOn = false;

    digitalWrite(_greenLed, LOW);
    digitalWrite(_redLed, LOW);
    digitalWrite(_r1, LOW);
    digitalWrite(_r2, LOW);
    digitalWrite(_r3, LOW);
    digitalWrite(_hazmatSuit, LOW);
    noTone(_buzzer);
    
}

void NuclearDisplay::roomOne()
{
    digitalWrite(_r1, HIGH);
    digitalWrite(_r2, LOW);
    digitalWrite(_r3, LOW);
}

void NuclearDisplay::roomTwo()
{
    digitalWrite(_r1, LOW);
    digitalWrite(_r2, HIGH);
    digitalWrite(_r3, LOW);
}

void NuclearDisplay::roomThree()
{
    digitalWrite(_r1, LOW);
    digitalWrite(_r2, LOW);
    digitalWrite(_r3, HIGH);
}

void NuclearDisplay::hazmat()
{
    if(_hazmatCurrentlyOn)
    {
        _hazmatCurrentlyOn = false;
        digitalWrite(_hazmatSuit, LOW);
    }
    else
    {
        _hazmatCurrentlyOn = true;
        digitalWrite(_hazmatSuit, HIGH);
    }
}

void NuclearDisplay::allOff()
{
    digitalWrite(_r1, LOW);
    digitalWrite(_r2, LOW);
    digitalWrite(_r3, LOW);
    digitalWrite(_hazmatSuit, LOW);
}

void NuclearDisplay::clockedIn()
{
    digitalWrite(_r1, HIGH);
    digitalWrite(_greenLed, HIGH);
    digitalWrite(_redLed, LOW);
    tone(_buzzer, 600, 100);
    delay(100);
    tone(_buzzer, 800, 100);
    delay(800);
    digitalWrite(_greenLed, LOW);
}

void NuclearDisplay::clockedOut()
{
    tone(_buzzer, 800, 100);
    digitalWrite(_greenLed, HIGH);
    delay(50);
    digitalWrite(_greenLed, LOW);
    delay(50);
    tone(_buzzer, 600, 100);
    digitalWrite(_greenLed, HIGH);
    delay(50);
    digitalWrite(_greenLed, LOW);
    delay(50);
}

void NuclearDisplay::accessDenied()
{
    digitalWrite(_redLed, HIGH);
    tone(_buzzer, 500, 500);
    delay(500);
    digitalWrite(_redLed, LOW);
}
