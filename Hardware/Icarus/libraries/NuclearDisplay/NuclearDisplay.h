/*
    NuclearDisplay.h
    Library for the LEDs and buzzer on the nuclear control panel
    Created 2019-10-03 by Edvin Egerhag
    Property of the Icarus project group
*/

#ifndef NuclearDisplay_h
#define NucelarDisplay_h

#include "Arduino.h"

class NuclearDisplay
{
    private: 
       int _r1;
       int _r2;
       int _r3;
       int _hazmatSuit;
       bool _hazmatCurrentlyOn;

       int _greenLed;
       int _redLed;
       int _buzzer;

    public:
        NuclearDisplay(int g, int r, int b, 
        int r1, int r2, int r3, int hazmat);
        void roomOne();
        void roomTwo();
        void roomThree();
        void hazmat();
        void allOff();
        void clockedIn();
        void clockedOut();
        void accessDenied();
};
#endif