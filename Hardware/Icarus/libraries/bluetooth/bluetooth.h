/*
 * bluetooth.h
 * This file contains the state machine that handles bluetooth communication.
 * Property of the Icarus project group
 *  */

#include <receivedAction.h>

void Bt()
{
  
  switch (BtState)
  {
    case BT_CHECK_STATE: 
      if (content.length() == 0)
      {
        //if there is nothing to send, read.
        BtState = BT_READ;
      }
      else
      {
        BtState = BT_WRITE;
      }
      break;

    case BT_READ:
      if (Serial.available())
      {
        //Saves incoming message sent from database, this message is then executed inside receivedAction()
        readMsg = Serial.read();
        receivedAction();
      }
      BtState = BT_CHECK_STATE;
      break;

    case BT_WRITE:
      //Sends outgoing message and resets message and message type variables.
      Serial.println(action + content);
      content = "";
      action = "";
      BtState = BT_CHECK_STATE;
      break;

    default:
      break;
  }
}
