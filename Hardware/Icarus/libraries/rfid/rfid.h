/**
 *  rfid.h
 * This file contains the state machine that checks for clock in attempts using an RFID card.
 * It makes use of the MFRC522 library that can be downloaded from https://github.com/miguelbalboa/rfid
 * Property of the Icarus project group
 */
void Rfid()
{
  switch (RfidState)
  {
    case RFID_WAIT:
      //if card is read set RfidState = RFID_GET_MSG
      if (!mfrc522.PICC_IsNewCardPresent())
        return;
      else
        RfidState = RFID_GET_MSG;
      break;

    case RFID_GET_MSG:
      //get message from read card and set RfidState = RFID_WRITE
      if (!mfrc522.PICC_ReadCardSerial())
      {
        RfidState = RFID_WAIT;
        return;
      }
      else
      {
        for (byte i = 0; i < mfrc522.uid.size; i++)
        {
          content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
          content.concat(String(mfrc522.uid.uidByte[i], HEX));
        }
        content.toUpperCase();
        mfrc522.PICC_HaltA();

        action = CARD_READ;
        RfidState = RFID_WAIT;
      }
      break;
      
    default:
      break;
  }
}
