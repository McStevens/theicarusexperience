/*
 * sendRadiation.h
 * this file contains a function that notifies the bluetooth state machine with an action and the current radiation.
 * Property of the Icarus project group.
  */
void sendRadiation()
{
  if(radiation.updateRadiation() && isClockedIn)
  {
    content = radiation.currentRadiation();
    action = SEND_RADIATION;
  }
}
