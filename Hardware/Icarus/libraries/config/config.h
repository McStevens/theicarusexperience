/*
* config.h
* This file contains the defines and variables used in the safety console program 
* */
#ifndef config_h
#define config_h

#include <SPI.h>            //Basic library used for SPI serial comms
#include <MFRC522.h>        //Library for the RFID card reader
#include <Radiation.h>      //Library for reading the potentiometer and returning a radiation value
#include <Buttons.h>        //Library that handles 4 pushbuttons on the safety console.
#include <NuclearDisplay.h> //Library that handles the LEDs and the buzzer on the safety console

//Definitions used when mapping pins on the arduino board
#define SS_PIN 10
#define RST_PIN 9
#define buttonPin1 7  
#define buttonPin2 6
#define buttonPin3 5
#define buttonPin4 14
#define buzzer 3
#define greenLED 15
#define redLED 16
#define yellowLED 4
#define blueLED1 17
#define blueLED2 18
#define blueLED3 2
#define potentiometer 19

#define SERIAL_BAUD_RATE 9600 //Used when starting the serial comms



MFRC522 mfrc522(SS_PIN, RST_PIN); //RFID card reader object, used for calling various functions relating to the RFID reader. Used in rfid.h
Radiation radiation(potentiometer); //Radiation object, needed when updating the radiation exposure. used in sendRadiation.h
Buttons buttons(buttonPin1, buttonPin2, buttonPin3, buttonPin4); //Button object used when checking button states.
//NuclearDisplay is used to control the various display outputs on the board. 
NuclearDisplay nuclearDisplay(greenLED, redLED, buzzer, blueLED1, 
                              blueLED2, blueLED3, yellowLED);

//Enums, used in the state machines.
enum RfidStates {RFID_WAIT, RFID_GET_MSG};
enum BtStates {BT_READ, BT_WRITE, BT_CHECK_STATE};
enum PushButtonStates {PUSHBUTTON_ROOM1, PUSHBUTTON_ROOM2, PUSHBUTTON_ROOM3, PUSHBUTTON_HAZMAT_SUIT, PUSHBUTTON_WAIT};


RfidStates RfidState = RFID_WAIT;
BtStates BtState = BT_CHECK_STATE;
PushButtonStates PushButtonState = PUSHBUTTON_WAIT;

//various variables used during execution.
bool hazmatSuitEquiped = false; 
bool isClockedIn = false;
String content = ""; //Essential variable, used to send different messages via bluetooth. Messages received during runtime   
char readMsg = 'a'; //used when receiving messages via bluetooth.
char action; //Used when sending messages via bluetooth, this variable explains what kind of message is sent.

//These are the different actions sent before the message. Tells the database what to do with the message.
char CARD_READ = '0';
char ROOM_CHANGE = '1';
char HAZMAT_SUIT = '2';
char SEND_RADIATION = '3';

//All the messages sent from the arduino board to the database
String ROOM_ONE = "1";
String ROOM_TWO = "2";
String ROOM_THREE = "3";
String HAZMAT_ON = "1";
String HAZMAT_OFF = "0";


#endif