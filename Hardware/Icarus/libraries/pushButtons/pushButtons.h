/*
 * pushButtons.h
 * This file contains the state machine that handles the buttons on the safety console
 * Property of the Icarus project group.
 */
void pushButtons()
{
  switch(PushButtonState)
  {
    case PUSHBUTTON_WAIT:
      
      if(buttons.isAnyButtonPressed() && isClockedIn)
        PushButtonState = buttons.currentButtonPressed();
      break;
      
    //room1
    case PUSHBUTTON_ROOM1:
      nuclearDisplay.roomOne();
      content = ROOM_ONE;
      action = ROOM_CHANGE;
      PushButtonState = PUSHBUTTON_WAIT;
      break;

    //room2
    case PUSHBUTTON_ROOM2:
      nuclearDisplay.roomTwo();
      content = ROOM_TWO;
      action = ROOM_CHANGE;
      PushButtonState = PUSHBUTTON_WAIT;
      break;

    //room3
    case PUSHBUTTON_ROOM3:
      nuclearDisplay.roomThree();
      content = ROOM_THREE;
      action = ROOM_CHANGE;
      PushButtonState = PUSHBUTTON_WAIT;
      break;
      
    case PUSHBUTTON_HAZMAT_SUIT:
      //if put on suit, set content = "1", if take off suit, set content = "1"
      if(!hazmatSuitEquiped)
      {
        nuclearDisplay.hazmat();
        hazmatSuitEquiped = true;
        content = HAZMAT_ON;
      }
      else if(hazmatSuitEquiped)
      {
        nuclearDisplay.hazmat();
        hazmatSuitEquiped = false;
        content = HAZMAT_OFF;
      }
      action = HAZMAT_SUIT;
      PushButtonState = PUSHBUTTON_WAIT;
      break;

    default:
      break;
  }
}
