/*
    Radiation.h
    library used for simulating radiation level 
    from 0% to 100%
    Created 2019-10-03 by Edvin Egerhag
    Property of Icarus Project group 
*/
#ifndef Radiation_h
#define Radiation_h

#define MAX_AD_INPUT 1024
#define MIN_AD_INPUT 0
#define MAX_RADIATION 100
#define MIN_RADIATION 1

#include "Arduino.h"

class Radiation
{
private:
    int _pin;
    int _radiation;
    int _oldRadiation;
    bool _doUpdate;
public:
    Radiation(int pin);
    int currentRadiation();
    bool updateRadiation();
};

#endif