/*
    Radiation.cpp
    library used for simulating radiation level 
    from 0% to 100%
    Created 2019-10-03 by Edvin Egerhag
    Property of Icarus Project group 
*/
#include "Radiation.h"
#include "Arduino.h"

Radiation::Radiation(int pin)
{
  _pin = pin;
  _oldRadiation = 0; 
  _radiation = 1;
  _doUpdate = false;
}

bool Radiation::updateRadiation()
{
  _doUpdate = false;
  _radiation = analogRead(_pin);

  _radiation = map(_radiation, MIN_AD_INPUT, MAX_AD_INPUT, 
  MIN_RADIATION, MAX_RADIATION);
  
  if(_radiation != _oldRadiation)
    _doUpdate = true;

  _oldRadiation = _radiation;
  
  return _doUpdate;
}

int Radiation::currentRadiation()
{
  delay(300);
  _radiation = analogRead(_pin);
  
 _radiation = map(_radiation, MIN_AD_INPUT, MAX_AD_INPUT, 
  MIN_RADIATION, MAX_RADIATION);

  return _radiation;
}
