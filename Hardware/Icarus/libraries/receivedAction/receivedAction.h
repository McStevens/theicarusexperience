/*
 * receivedAction.h
 * This file contains the state machine that handles messages received from the database.
 * executes commands depending in the message.
 * Property of the Icarus project group. 
 * */
void receivedAction()
{
  switch (readMsg)
  {
    case '0': //After tapping an invalid RFID card, the database denies the clock in.
      nuclearDisplay.accessDenied();
      break;
   
    case '1': //After tapping a valid RFID card, the database verifies the clock in.
      nuclearDisplay.clockedIn();
      nuclearDisplay.roomOne();
      action = SEND_RADIATION;
      content = radiation.currentRadiation();
      Serial.println(action + content);
      content = ROOM_ONE;
      action = ROOM_CHANGE;
      isClockedIn = true;
      break;
    
    case '2': // After clocking out, the database notifies the safety console.
      nuclearDisplay.clockedOut();  
      nuclearDisplay.allOff();
      isClockedIn = false;
      break;

    default:
      break;
  }
  readMsg = 'a';
}
