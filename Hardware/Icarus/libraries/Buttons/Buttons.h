/*
    Buttons.h
    Library for checking the buttons on the nuclear control panel
    Created 2019-10-03 by Edvin Egerhag
    Property of the Icarus project group
*/
#ifndef Buttons_h
#define Buttons_h

#include "Arduino.h"

class Buttons
{
    private:
        int _button0;
        int _button1;
        int _button2;
        int _button3;

        int _b0State;
        int _b1State;
        int _b2State;
        int _b3State;

        int _b0PrevState;
        int _b1PrevState;
        int _b2PrevState;
        int _b3PrevState;

        bool isPressed;
    public:
        Buttons(int b0, int b1, int b2, int b3);

        bool isAnyButtonPressed();
        int currentButtonPressed();
};
#endif
