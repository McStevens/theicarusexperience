/*
    Buttons.cpp
    Library for checking the buttons on the nuclear control panel
    Created 2019-10-03 by Edvin Egerhag
    Property of the Icarus project group
*/

#include "Buttons.h"
#include "Arduino.h"

Buttons::Buttons(int b0, int b1, int b2, int b3)
{
   _button0 = b0;
   _button1 = b1;
   _button2 = b2;
   _button3 = b3; 
   pinMode(_button0, INPUT);
   pinMode(_button1, INPUT);
   pinMode(_button2, INPUT);
   pinMode(_button3, INPUT);

   _b0State = 0;
   _b1State = 0;
   _b2State = 0;
   _b3State = 0;

   _b0PrevState = 0;
   _b1PrevState = 0;
   _b2PrevState = 0;
   _b3PrevState = 0;
}

bool Buttons::isAnyButtonPressed()
{
    _b0State = digitalRead(_button0);
    _b1State = digitalRead(_button1);
    _b2State = digitalRead(_button2);
    _b3State = digitalRead(_button3);

    isPressed = false;

    if(_b0State != _b0PrevState)
     {
        if(_b0State)
            isPressed = true;
     }
     if(_b1State != _b1PrevState)
     {
        if(_b1State)
             isPressed = true;
     }
     if(_b2State != _b2PrevState)
     {
        if(_b2State)
             isPressed = true;
     }
     if(_b3State != _b3PrevState)
     {
        if(_b3State)
             isPressed = true;
     }
     _b0PrevState = _b0State;
     _b1PrevState = _b1State;
     _b2PrevState = _b2State;
     _b3PrevState = _b3State;
     return isPressed;
}

int Buttons::currentButtonPressed()
{
    if(_b0State)
        return 0;
    else if(_b1State)
        return 1;
    else if(_b2State)
        return 2;
    else if(_b3State)
        return 3;
}