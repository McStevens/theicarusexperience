import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View } from 'react-native';
import {TimeLeft, ClockInComponent, HistoryModal, Button} from './components';
import * as Logic from './components/Logic.js';
import { BlProtocol } from './components/Constants.js';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android: 'Double tap R on your keyboard to reload,\n' + 'Shake or press menu button for dev menu',
});

const defaultState = {
  user: { 
          id: '', 
          name: '', 
          role: ''
        },
  loading: false,
  blStatus: 'NONE',
  clockedInAt : '',
  keyId : '',
  room: {
    id: '1',
    roomCoefficient: '0.1',
    roomName: 'Break room'
  },
  isSuitEquipped : false,
  isTimerOn : false,
  employeRole: "",
  confirmClockIn: false,
  confirmClockOut: false,
  error: '',
  radPerSec: 30,
  suitEvents: [],
  roomEvents: [],
  radiationEvents: []
}

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = defaultState;
  }

  onClockIn = () => {
    this.setState({loading: true}, () => {
      Logic.clockIn(this.state.keyId)
      .then(staffInfo => {
        this.setState({
          user: { 
                ...staffInfo 
              }, 
          confirmClockIn : false, 
          loading: false,
          isTimerOn: true,
        })
      })
      .catch(error => {
        this.setState({
          loading: false, 
          error : error.message || error
        })
      }) 
    })
  }

  onClockOut = () => {
    Logic.clockOut(
      this.state.keyId, 
      this.state.user, 
      {
        isSuitEquipped: this.state.suitEvents,
        roomEvents: this.state.roomEvents,
        radiationEvents: this.state.radiationEvents
      })
    .then(() => {
      this.setState({
        ...defaultState, 
        confirmClockOut: false,
        isTimerOn: false
      });
    })
    .catch(error => {
    })
  }

  onBlueToothStatusChange = (status) =>  {
    if(this.state.blStatus == 'CONNECTED' && status == 'NONE'){
      Logic.initBl(this.onBlueToothStatusChange, this.onBlueToothDataRead) 
      this.setState({blStatus : status})
    } else {
      this.setState({blStatus : status})
    }
  }

  onBlueToothDataRead = (code, message) => {
    switch(code) {
      case BlProtocol.BLIP:
        Logic.checkIfClockedIn(message)
        .then((clockedIn) => {
          this.setState({ keyId: message, 
            confirmClockOut: clockedIn, 
            confirmClockIn: !clockedIn
          })
        })
        .catch(error => {
          this.setState({
            keyId: '',
            error
          })
        })
        break;
      case BlProtocol.ROOMCHANGE:
        const roomId = message;
        if(this.state.room.id != roomId) {
          Logic.getRoomData(roomId)
          .then(room => {
            const roomEvents = [...this.state.roomEvents];
            roomEvents.push({
              currentRoom: roomId,
              timedAt: Date.now()
            });
            this.setState({
              room, 
              roomEvents
            })
          })
        }
         else {
          console.log("Already in the same room")
        }
        break;
      case BlProtocol.SUIT:
        const suitEvents = [...this.state.suitEvents];
        if(message === "1"){
          suitEvents.push({suitStatus: "Equiped", timedAt: Date.now()})
          this.setState({
            isSuitEquipped: true,
            suitEvents
          })
        } else if(message === "0") {
          suitEvents.push({suitStatus: "Not equiped", timedAt: Date.now()})
          this.setState({
            isSuitEquipped: false,
            suitEvents
          })
        } else {
          console.log("Invalid suit action")
        }
        break;
      case BlProtocol.RADIATION:
        if(this.state.radPerSec != message && this.state.isTimerOn) {
          const radiationEvents = [...this.state.radiationEvents];
          radiationEvents.push({radiationPerSecond: message, timedAt: Date.now()})
          this.setState({
            radPerSec: parseFloat(message),
            radiationEvents
          })
        }
        break;
      case BlProtocol.SYSTEM:
        break;
      default:
        console.log("Status code outside of protocol: ", code)
      break;
    }
  }

  componentDidMount() {
    Logic.initBl(this.onBlueToothStatusChange, this.onBlueToothDataRead) 
  }

  
  render() {
    const { blStatus, user, room, confirmClockIn, confirmClockOut, error, isTimerOn, isSuitEquipped, radPerSec} = this.state;
    return (
      <>
      <View style={styles.topView}>
        <TimeLeft radPerSec={radPerSec} roomCoefficient={room.roomCoefficient} isTimerOn={isTimerOn} isSuitEquipped={isSuitEquipped} />
      </View>

      <View style={styles.middleView}>
        <Text style={styles.roomText}>Current room: {room.id}</Text>
        <Text style={styles.roomText}>Room name: {room.roomName}</Text>
        <Text style={styles.roomText}>Role: {user.role} </Text>
        <Text style={styles.roomText}>Suit: {isSuitEquipped ? "equipped" : "not equipped"} </Text>
        {/* <Text style={styles.roomText}>blStatus : {blStatus}</Text> */}
      </View>

      <View style={styles.bottomView}>

        <ClockInComponent 
          confirmClockIn={confirmClockIn} 
          confirmClockOut={confirmClockOut} 
          onConfirm={
            () => confirmClockIn 
            ? this.onClockIn() 
            : this.onClockOut()
          } 
        />
      </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  topView: {
    paddingTop: 35,
    backgroundColor: 'white',
    height: "45%",
    borderBottomWidth: 2,
    borderColor: "black"
  },
  middleView: {
    paddingTop: 5,
    backgroundColor: 'white',
    height: "25%",
    borderBottomWidth: 2,
    borderColor: "black"
  },
  bottomView: {
    paddingTop: 5,
    backgroundColor: 'white',
    height: "30%"
  },
  warningText: {
    color: "red",
    fontSize: 45
  },
  title: {
    fontSize: 40,
    textAlign: "center"
  },
  subTitle: {
    fontSize: 30,
    textAlign: "center"
  },
  timerTitle: {
    fontSize: 80,
    textAlign: "center"
  },
  inlineTextView: {
    marginTop: 10,
    flexDirection: 'row',
    alignItems: 'center'
  },
  roomText: {
    textAlign: "left",
    marginLeft: 15,
    fontSize: 20
  },
  customText: {
      fontSize: 16,
      textTransform: 'none',
      color: '#FFFFFF',
  },
  clockButton:{
    marginBottom: 30,
    marginLeft: "20%",
    marginRight: "20%",
    backgroundColor: "transparent",
    padding: 10,
    height: 80,
    borderColor: "black",
    borderRadius: 50,
    borderWidth: 2
  },
  clockButtonText:{
    fontSize: 45,
    textAlign: "center",
    color: "black"
  }
});
