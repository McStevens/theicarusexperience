import {clockIn, clockOut, getRoomData} from '../components/Logic'
import { errorCodes } from '../components/Constants';
import 'react-native';
jest.mock('easy-bluetooth-classic', () => ({ writeln: jest.fn(() => {}) }));
jest.mock('../components/API.js', () => ({ 
    clockOut: jest.fn((staffId) => {return new Promise((resolve, reject) => {
        staffId == '0' ? resolve() : reject('Not Clocked int')});
    }),  
    getClockInData: jest.fn((staffId) => {return new Promise((resolve, reject) => {
        staffId == '0' ? resolve({}) : reject('Not Clocked int')});
    }),     
    storeClockInEvent: jest.fn(() => {return Promise.resolve()}),
    checkId: jest.fn((keyId) => {return new Promise((resolve, reject) => {
        keyId == 'C9E69355' ? resolve('0') : reject('Not Found')});
    }),
    clockIn: jest.fn(() => {return Promise.resolve()}),
    getStaff: jest.fn((staffId) => {return new Promise((resolve, reject) => {
        staffId == '0' ? resolve({name: 'Bjorn'}) : reject('Not Clocked int')});
    }), 
    getRoomData: jest.fn((roomId) => {return new Promise((resolve, reject) => {
        roomId == 1 ? resolve({roomCoefficient: 0.1, roomName: 'Break room'}) : reject('Room not Found')});
    }),     
}));

describe("Logic methods", () => {
    
    test('clockIn should return staffInfo', () => {
        const expected = { name: 'Bjorn'}
        return expect(clockIn('C9E69355')).resolves.toMatchObject(expected);
    });

    test('clockIn should return error', () => {
        return expect(clockIn('')).rejects.toThrow();
    });


    test('clockOut should succeed', () => {
        return expect(clockOut('0')).resolves.toBe(true);
    });

    test('clockOut should fail', () => {
        return expect(clockOut('C9E69355', {keyId:'sdfsdf'})).rejects.toThrow();
        
    });

    test('getRoomData should return roomData', () => {
        const expected = {roomCoefficient: 0.1, roomName: 'Break room'};
        return expect(getRoomData(1)).resolves.toMatchObject(expected);
    });

    test('getRoomData should throw', () => {
        return expect(getRoomData(10)).rejects.toThrow();
        
    });

  });