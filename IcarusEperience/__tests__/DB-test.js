/**
 * @format
 */

import 'react-native';
import * as api from '../components/API';

jest.mock('easy-bluetooth-classic')

it('checkId should return staffId', () => {
  return expect(api.checkId('C9E69355')).resolves.toBe(0);
});

it('checkId should return error', () => {
  return expect(api.checkId('adfasdf')).rejects.toMatch('Not Found');
});

test('getRoomData should return roomdata',  () => {
  const expected = {roomCoefficient: 0.1, roomName: 'Break room'};
  return expect(api.getRoomData(1)).resolves.toMatchObject(expected);
})

test('getRoomData should throw',  () => {
  return expect(api.getRoomData('10')).rejects.toMatch('Room not Found');
})


