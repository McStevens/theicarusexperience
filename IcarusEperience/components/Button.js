import React, { Component } from 'react';
import { Text, View,StyleSheet, TouchableOpacity } from 'react-native';



export const Button = (props) => {
    const { title = 'NO TEXT SET >:(', style = styles.button, textStyle = styles.text, onPress } = props;

    return(
        <TouchableOpacity onPress={onPress} style={style}>
            <Text style={textStyle}>{title}</Text>
        </TouchableOpacity>
        )
}

const styles = StyleSheet.create({
    button: {
        display: 'flex',
        height: 50,
        borderRadius: 5,
        justifyContent: 'center',
        alignItems: 'center',

        backgroundColor: '#2AC062',
        shadowColor: '#2AC062',
        shadowOpacity: 0.4,
        shadowOffset: { height: 10, width: 0 },
        shadowRadius: 20,
    },

    text: {
        fontSize: 16,
        textTransform: 'uppercase',
        color: '#FFFFFF',
    },
});