import {PermissionsAndroid} from 'react-native';
import EasyBluetooth from 'easy-bluetooth-classic';

    const config = {
      "uuid": "00001101-0000-1000-8000-00805F9B34FB",
      "deviceName": "Bluetooth Example Project",
      "bufferSize": 1024,
      "characterDelimiter": "\n"
    }

  async function requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'location',
          message:
            'location Access Requiered ' +
            '',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('Access Granted');
      } else {
        console.log('Access Denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  export const Connect = (onStatusChange, onDataRead) => {
    requestLocationPermission();

    this.onDeviceFoundEvent = EasyBluetooth.addOnDeviceFoundListener(onDeviceFound);
    this.onStatusChangeEvent = EasyBluetooth.addOnStatusChangeListener(onStatusChange);
    this.onDataReadEvent = EasyBluetooth.addOnDataReadListener(onDataRead);

    EasyBluetooth.init(config)
    .then(function (config) {
      console.log("config done!");
    })
    .catch(function (ex) {
      console.warn(ex);
    })
    EasyBluetooth.startScan()
    .catch(function (ex) {
        console.warn(ex);
    })
  }

   export const Disconnect = () => {
        this.onDeviceFoundEvent.remove();
        this.onStatusChangeEvent.remove();
        this.onDataReadEvent.remove();
    }

   export const writeData = (data) => {
        EasyBluetooth.writeln(data);
                
    }

    const onDeviceFound = (device) => {
      if(device.address === '98:D3:81:FD:49:37'){
        EasyBluetooth.connect(device)
            .catch((ex) => {
                console.warn(ex);
            })
      }      
    }
    
 