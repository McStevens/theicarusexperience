import React, { Component } from 'react';
import { Text, StyleSheet, Vibration } from 'react-native';


//calculates the ammount of exposure the worker gets per second in the current room, and with/without suit.
export const currentExposurePerSec = (radPerSec, roomCoefficient, suitEquipped) => { 
    console.log("roomcoefficient: ", roomCoefficient);
    return (radPerSec * roomCoefficient) / (suitEquipped ? 5 : 1);
}

//Calculates how much time the worker can spend in the current room.
// export const calculateTimeLeft = (currentExposure, currentExposurePerSec) => {
//     const exposureLimit = 500000;
//     var timeLeft = (exposureLimit - currentExposure)/currentExposurePerSec;
//     return convertToTimeString(timeLeft)
// }

// export const convertToTimeString = (totalSeconds) => 
// {
//     var hours = Math.floor(totalSeconds / 3600);
//     totalSeconds %= 3600;
//     var minutes = Math.floor(totalSeconds / 60);
//     var seconds = Math.floor(totalSeconds % 60);

//     if(hours < 0)
//         hours = 0;
//     if(minutes < 0)
//         minutes = 0;
//     if(seconds < 0)
//         seconds = 0;

//     if(hours == Infinity || minutes == Infinity || seconds == Infinity)
//         return "Calculating"
//     else
//         return hours + "h " + minutes + "m " + seconds + "s";
// }

//return the time left for the worker in the current room, supposed to update each second.
export class TimeLeft extends Component {
    state = {
        time: 0,
        exposurePerSec: null,
        isTimerOn: this.props.isTimerOn,
        timerToken: null,
        warning: false,
        pastExposure: null
    };

    convertToTimeString = (totalSeconds) => 
    {
        var hours = Math.floor(totalSeconds / 3600);
        totalSeconds %= 3600;
        var minutes = Math.floor(totalSeconds / 60);
        var seconds = Math.floor(totalSeconds % 60);
        
        if(hours < 0)
        hours = 0;
        if(minutes < 0)
        minutes = 0;
        if(seconds < 0)
        seconds = 0;
        
        if(hours == Infinity || minutes == Infinity || seconds == Infinity)
            return "Calculating"
        else
            return hours + "h " + minutes + "m " + seconds + "s";
    }

    calculateTimeLeft = (currentExposurePerSec) => {
        const exposureLimit = 500000;
        var timeLeft = (exposureLimit - this.state.pastExposure - this.calculateExposure(currentExposurePerSec))/currentExposurePerSec;

        this.warningVibration(10,timeLeft,3600)
        console.log("timeLeft: ", timeLeft)
        return this.convertToTimeString(timeLeft)
    }

    warningVibration = (vibrationInterval,timeLeft,vibrateFrom) => {
        if(timeLeft <= vibrateFrom && Math.round(timeLeft) % vibrationInterval == 0 && timeLeft >= 0){
            Vibration.vibrate(500)
        }
    }
    
    componentDidUpdate(prevProps, prevState) {
        if(this.props.isTimerOn && prevProps.isTimerOn == false){
            if(this.state.timerToken != null)
            {
                this.stopTimer();
            }
            this.startTimer()
        }
        else if(!this.props.isTimerOn && prevProps.isTimerOn == true){
            if(this.state.timerToken != null){
                this.stopTimer()
            }
        }
        if(!this.props.isTimerOn || this.props.isTimerOn == null){
            clearInterval(this.state.timerToken)
        }
        if(this.state.exposurePerSec != prevState.exposurePerSec && this.props.isTimerOn) {
            this.setState({
                pastExposure: (this.state.pastExposure + this.calculateExposure(prevState.exposurePerSec)),
                time: 0
            })
        }
    }

    startTimer = () => {
        const timerToken = setInterval(()=>{
            this.setState({
                time: this.state.time + 1,
                exposurePerSec: currentExposurePerSec(this.props.radPerSec, this.props.roomCoefficient, this.props.isSuitEquipped),
                isTimerOn: true
            })
        },1000)
        this.setState({
            timerToken: timerToken
        })
    }

    stopTimer = () => {
        clearInterval(this.state.timerToken)
        this.setState({
            isTimerOn: false,
            timerToken: null
        })
    }

    clearTimer = () =>{
        this.setState({
            time: 0
        })
    }

    calculateTotalExposure = () => {
        return (this.state.pastExposure + (this.state.exposurePerSec * this.state.time))
    } 

    calculateExposure = (exposurePerSec) => {
        return (exposurePerSec * this.state.time)
    }   

    render(){        
        return(
            <>
                <Text style={styles.title}>Total Exposure: {Math.floor(this.calculateTotalExposure())}</Text>
                <Text style={styles.esttime}>Estimated time left:</Text>
                <Text style={styles.time}>{this.calculateTimeLeft(this.state.exposurePerSec)} </Text>
                <Text style={styles.text}>Current Exposure/s: {this.state.exposurePerSec}</Text>
                <Text style={styles.text}>Time spent in room: {this.convertToTimeString(this.state.time)}</Text>
                <Text style={styles.warningText}>{this.state.warning ? "Warning, approaching lethal dose of radiation!" : " "}</Text> 
            </>
        )
    }
}

const styles = StyleSheet.create({
    time: {
        fontSize: 50,
        textAlign: "center"
    },
    title: {
        fontSize: 25,
        textAlign: "center"
    },
    esttime: {
        marginTop: 20,
        marginBottom: -10,
        fontSize: 25,
        fontWeight: "bold",
        textAlign: "center"
    },
    text: {
        marginLeft: 30
    },
    warningText: {
        color: "red",
        fontSize: 20,
        marginLeft: 3
      }
});
