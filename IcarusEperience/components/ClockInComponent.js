import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import {Button} from "./Button";

export class ClockInComponent extends Component {
    state = {
        confirmClockIn: this.props.confirmClockIn,
        confirmClockOut: this.props.confirmClockOut        
    }


    componentDidUpdate(prevProps)
    {
        if(prevProps.confirmClockIn != this.props.confirmClockIn){
            this.setState({
                confirmClockIn: this.props.confirmClockIn
            })
        }
        else if(prevProps.confirmClockOut != this.props.confirmClockOut){
            this.setState({
                confirmClockOut: this.props.confirmClockOut
            })
        }
    }

    render(){


        if(this.state.confirmClockIn){
            return(
                <>
                    <Text style={styles.questionText}>Please confirm clock-in</Text>
                    <Button style={styles.yesButton} textStyle={styles.yesButtonText} onPress={this.props.onConfirm} title="Confirm"/>
                </>
            )
        }else if(this.state.confirmClockOut){
            return(
                <>
                    <Text style={styles.questionText}>Confirm clock-out</Text>
                    <Button style={styles.yesButton} textStyle={styles.yesButtonText} onPress={this.props.onConfirm} title="Confirm"/>
                </>
            )
        }
        else
        {
            return(
                <>
                
                </>
            )
        }
    }
}
const styles = StyleSheet.create({
    yesButton:{
        margin: 15,
        backgroundColor: "lightgreen",
        height: 45,        
        marginLeft: "25%",
        marginRight: "25%",
        borderWidth: 3,
        borderRadius: 3,
        borderColor: "black"
    },
    yesButtonText:{
        fontSize: 35,
        textAlign: "center",
        color: "black"
    },
    questionText:{
        fontSize: 20,
        textAlign: "center"
    }
});