import * as firebase from 'firebase';

// Initialize Firebase
const firebaseConfig = {
  apiKey: "AIzaSyASzJbMW7tQ1BYdkNJ1hZDxo5GsZoKTHAk",
  authDomain: "icarus-94c7e.firebaseapp.com",
  databaseURL: "https://icarus-94c7e.firebaseio.com",
  storageBucket: "icarus-94c7e.appspot.com"
};

firebase.initializeApp(firebaseConfig);

export const Database = firebase.database();