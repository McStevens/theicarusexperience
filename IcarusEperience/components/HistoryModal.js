import React, {Component} from 'react';
import {Modal, Text, TouchableHighlight, StyleSheet , View, Alert} from 'react-native';
import {Button} from './Button';

export class HistoryModal extends Component {
  state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  }

  render() {
    return (
      <View style={{marginTop: 22}}>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            this.setModalVisible(!this.state.modalVisible);
          }}>
            <View style={styles.topView}>
                <Text>Hello World!</Text>

                <Button title="haha yes"/>
            </View>
            <View style={styles.bottomView}>
              <Button
                title="Close"
                style={styles.customButton}
                onPress={() => {
                    this.setModalVisible(!this.state.modalVisible);
                }}>
              </Button>
            </View>
        </Modal>

        <Button
            style={styles.customButton}
            title="History"
            onPress={() => {
                this.setModalVisible(true);
          }}>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    customButton: {
        height: 100,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: "35%",
        marginRight: "35%",
        backgroundColor: '#2AC062',
        alignContent:"center",
        bottom: 0,
        marginBottom: 36,
        position: "relative",
    },
    topView: {
        flex: 4,
        padding: "10%",
        backgroundColor: "red"
    },
    bottomView: {
        flex: 1,
        justifyContent: "flex-end",
        paddingTop: 5,
        backgroundColor: 'white',
        backgroundColor: "green"
      },

});