import * as api from './API';
import * as bl from './blclassic';
import { BlProtocol, statusCodes } from './Constants';

export const initBl = (onStatusCHange, onDataCange) => {
    bl.Connect(
        (status) => onStatusCHange(status), 
        (data) => {
            const code = data[0];
            const message = data.substring(1).replace(/\s/g, "");;
            onDataCange(code, message)
        }
    );
}

export const RoomClockIn = (staffId) => {
    const timeStamp = Date.now();
  Database.ref('RoomClockedIn/'+staffId).set({clockIn: timeStamp})
  .then(() => {
        //callback true
  })
  .catch(error => ({
    //callback error
  })
  )
}

export const clockIn = async (keyId) => {
    
    try{
        console.log("Keyid: ", keyId)   
        const staffId = await api.checkId(keyId); 
        console.log("staffid")   
        const clockedInAt = Date.now();
        await api.clockIn({staffId, clockedInAt});
        const staffInfo = await api.getStaff(staffId);
        staffInfo.clockedInAt = clockedInAt;
        bl.writeData(statusCodes.clockIn);
        return staffInfo;
    } 
    catch(e){
        bl.writeData(statusCodes.fail);
         console.log("ClockIn ERROR:", e);
        throw Error(e)
    }
}

export const clockOut = async (keyId, staff, eventData) => {
    try{
        if(staff.keyId && keyId != staff.keyId)
            throw("Wrong keyId")
        const clockInData = await api.getClockInData(staff.id);
        await api.clockOut(staff.id);
        const clockedOutAt =  Date.now();
        bl.writeData(statusCodes.clockOut);
        await api.storeClockInEvent({...clockInData, ...eventData, clockedOutAt});
    } 
    catch(e){
        bl.writeData(statusCodes.fail);
        console.log(e);
        throw Error(e)
    }
}

export const checkIfClockedIn = async (keyId) => {   
    try{
        const staffId = await api.checkId(keyId);  
        await api.getClockInData(staffId);
        return true;
    } 
    catch(e){
       if(e == 'Not Found') {
           bl.writeData(statusCodes.fail)
           throw(e);
       } else{
        return false;
       }      
    }    
}

export const getRoomData = async (roomId) => {

    try {
        const roomData = await api.getRoomData(roomId);
        return roomData;
    }
    catch(e){
        throw Error(e);
    }
}

