import { Database } from './db';

  export const getStaff = (staffId) => {
    return new Promise((resolve, reject) => {
        Database.ref('staff/'+ staffId).once('value')
        .then(snapshot => {
            resolve(snapshot.val())
        }) 
        .catch(error => {
            console.log(error);
            reject();
        })
    })
  }    

    

export const RoomClockIn = (staffId) => {
    return new Promise((resolve, reject) => {
        const timeStamp = Date.now();
        Database.ref('RoomClockedIn/'+staffId).set({clockIn: timeStamp})
        .then(() => {
              //callback true
        })
        .catch(error => ({
          //callback error
        })
        )
    })    
}

export const clockIn = (clockInData) => {

    return new Promise((resolve, reject) => {   
        Database.ref('ClockedIn/'+clockInData.staffId).set(clockInData)
        .then(() => {
            resolve();
        })
        .catch(error => {
            console.log(error);
            reject();
        }
        )
    })    
}

export const clockOut = (staffId) => {
    return new Promise((resolve, reject) => {
        Database.ref('ClockedIn/'+staffId).remove()
        .then(() => {       
          resolve();     
        })
        .catch(error => {
            console.log(error)
            reject();
        })  
    })    
}
  
  export const checkId = (number) => {
    if(!number) return Promise.reject('Not Found');
    return new Promise((resolve, reject) => {
        Database.ref('keyIds/'+number).once('value')
        .then(snapshot => { 
              if(snapshot.exists())  {
                resolve(snapshot.val());
              }  else{
                reject('Not Found');
              }             
            })
            .catch(error =>{
                console.log(error)
              reject('Not Found');
            })    
    })    
  }

  export const storeClockInEvent = (eventData) => {
    return new Promise((resolve, reject) => {
        Database.ref('events/clockIn').push(eventData)
        .then(() => {            
              resolve();
            })
            .catch(error =>{
                console.log(error)
             reject();
           })    
    })    
  }

  export const getClockInData = (staffId) => {
    return new Promise((resolve, reject) => {
        Database.ref('ClockedIn/'+staffId).once('value')
        .then((snapshot) => {  
          if(snapshot.exists())  {
            resolve(snapshot.val());
          }  else{
            reject('Not Clocked int');
          }                   
        })
        .catch(error => {
            console.log(error)
            reject('Not Clocked int');
        })  
    })    
}

export const getRoomData = (roomId) => {
  return new Promise((resolve, reject) => {
    Database.ref('rooms/'+roomId).once('value')
    .then((snapshot) => {  
      if(snapshot.exists())  {
        resolve(snapshot.val());
      }  else{
        reject('Room not Found');
      }                   
    })
    .catch(error => {
        console.log(error)
        reject('Room not found');
    })  
})    
}