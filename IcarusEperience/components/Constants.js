
export const BlProtocol = {
    BLIP: "0",
    ROOMCHANGE: "1",
    SUIT: "2",
    RADIATION: "3",
    SYSTEM: "4"
}

export const statusCodes = {
    fail: "0",
    clockIn: "1",
    clockOut: "2"
}