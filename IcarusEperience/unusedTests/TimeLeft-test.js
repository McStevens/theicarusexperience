/**
 * @format
 */
import 'react-native';
import * as timeLeft from '../components/TimeLeft'

// test currentExposurePerSec
it('currentExposurePerSec should return 9.6', () => {
    return expect(timeLeft.currentExposurePerSec(30, 1.6, true)).toBe(9.6);
});

it('currentExposurePerSec should return 48', () => {
    return expect(timeLeft.currentExposurePerSec(30, 1.6, false)).toBe(48);
});

//test calculateTimeLeft
it('calculateTimeLeft should return 5sec', () => {
    return expect(timeLeft.calculateTimeLeft(0, 100000)).toBe("0h 0m 5s");
});

it('calculateTimeLeft should return 10 sec', () => {
    return expect(timeLeft.calculateTimeLeft(0, 50000)).toBe("0h 0m 10s");
});

//test convertToTimeString
it('convertToTimeString should return 10 sec', () => {
    return expect(timeLeft.convertToTimeString(10)).toBe("0h 0m 10s");
});

it('calculateTimeLeft should return 7 min', () => {
    return expect(timeLeft.convertToTimeString(420)).toBe("0h 7m 0s");
});



